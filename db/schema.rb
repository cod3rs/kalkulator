# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150718171037) do

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.text     "te"
    t.text     "ld"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "heatings", force: :cascade do |t|
    t.integer  "category"
    t.string   "name"
    t.string   "efficiency_label"
    t.integer  "efficiency"
    t.string   "efficiency_measure"
    t.string   "price_label"
    t.decimal  "price"
    t.string   "price_measure"
    t.string   "value_label"
    t.decimal  "value"
    t.string   "value_measure"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "material_constructions", force: :cascade do |t|
    t.string   "name"
    t.decimal  "thermal_conductivity"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "material_thermal_insulations", force: :cascade do |t|
    t.string   "name"
    t.decimal  "thermal_conductivity"
    t.decimal  "purchasing_cost"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

end
