file = File.read('data/heating.json')
heating_data = JSON.parse(file)

heating_data.each do |data|
  heating = Heating.new
  heating.category = data['type']
  heating.name = data['name']

  heating.efficiency_label = data['efficiency_label']
  heating.efficiency = data['efficiency']
  heating.efficiency_measure = data['efficiency_measure']

  heating.price_label = data['price_label']
  heating.price = data['price']
  heating.price_measure = data['price_measure']

  heating.value_label = data['value_label']
  heating.value = data['value']
  heating.value_measure = data['value_measure']
  heating.save!
end

file = File.read('data/temperatures.json')
temperatures_data = JSON.parse(file)

temperatures_data.each do |data|
  city = City.new
  city.name = data['city']

  city.te = data['te']
  city.ld = data['ld']
  city.save!
end

file = File.read('data/material_construction.json')
material_construction_data = JSON.parse(file)

material_construction_data.each do |data|
  material_construction = MaterialConstruction.new
  material_construction.name = data['name']

  material_construction.thermal_conductivity = data['thermal_conductivity']
  material_construction.save!
end

file = File.read('data/material_thermal_insulation.json')
material_thermal_insulation_data = JSON.parse(file)

material_thermal_insulation_data.each do |data|
  material_thermal_insulation = MaterialThermalInsulation.new
  material_thermal_insulation.name = data['name']

  material_thermal_insulation.thermal_conductivity = data['thermal_conductivity']
  material_thermal_insulation.purchasing_cost = data['cost_of_purchasing']
  material_thermal_insulation.save!
end
