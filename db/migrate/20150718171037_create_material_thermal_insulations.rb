class CreateMaterialThermalInsulations < ActiveRecord::Migration
  def change
    create_table :material_thermal_insulations do |t|
      t.string :name
      t.decimal :thermal_conductivity
      t.decimal :purchasing_cost

      t.timestamps null: false
    end
  end
end
