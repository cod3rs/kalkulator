class CreateHeatings < ActiveRecord::Migration
  def change
    create_table :heatings do |t|
      t.integer :category
      t.string :name

      t.string :efficiency_label
      t.integer :efficiency
      t.string :efficiency_measure

      t.string :price_label
      t.decimal :price
      t.string :price_measure

      t.string :value_label
      t.decimal :value
      t.string :value_measure

      t.timestamps null: false
    end
  end
end
