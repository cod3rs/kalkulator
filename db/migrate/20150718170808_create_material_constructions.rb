class CreateMaterialConstructions < ActiveRecord::Migration
  def change
    create_table :material_constructions do |t|
      t.string :name
      t.decimal :thermal_conductivity

      t.timestamps null: false
    end
  end
end
