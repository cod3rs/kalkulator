class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.text :te
      t.text :ld

      t.timestamps null: false
    end
  end
end
