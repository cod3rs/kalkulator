var TemperatureCollection = Backbone.Collection.extend({
  model: TemperatureModel,
  url: '/api/cities'
});

var temperatureCollection = new TemperatureCollection();
