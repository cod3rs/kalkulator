var MaterialConstructionCollection = Backbone.Collection.extend({
  model: MaterialConstructionModel,
  url: '/api/material_constructions'
});

var materialConstructionCollection = new MaterialConstructionCollection();
