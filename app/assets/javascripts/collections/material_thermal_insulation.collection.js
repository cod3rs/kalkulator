var MaterialThermalInsulationCollection = Backbone.Collection.extend({
  model: MaterialThermalInsulationModel,
  url: '/api/material_thermal_insulations'
});

var materialThermalInsulationCollection = new MaterialThermalInsulationCollection();
