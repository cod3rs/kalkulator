var HeatingTypeCollection = Backbone.Collection.extend({
  model: HeatingTypeModel,
  url: '/data/heating_types.json'
});

var heatingTypeCollection = new HeatingTypeCollection();
