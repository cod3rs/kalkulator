var HeatingCollection = Backbone.Collection.extend({
  model: HeatingModel,
  url: '/api/heating'
});

var heatingCollection = new HeatingCollection();
