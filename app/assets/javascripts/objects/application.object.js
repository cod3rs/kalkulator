var ApplicationObject = Marionette.Object.extend({
  heating: function() {
    console.log('ApplicationController::heating');

    var complete = _.invoke([heatingCollection], 'fetch');

    $.when.apply($, complete).done(function() {
      heatingLayoutView.render();
    });
  },

  temperature: function() {
    console.log('ApplicationController::temperature');

    var complete = _.invoke([temperatureCollection], 'fetch');

    $.when.apply($, complete).done(function() {
      temperatureView.render();
    });
  },

  area: function() {
    console.log('ApplicationController::area');
    areaView.render();
  },

  material: function() {
    console.log('ApplicationController::material');

    var complete = _.invoke([materialConstructionCollection, materialThermalInsulationCollection], 'fetch');

    $.when.apply($, complete).done(function() {
      materialView.render();
    });
  },

  result: function() {
    console.log('ApplicationController::result');

    var complete = _.invoke([resultModel], 'fetch');

    $.when.apply($, complete).done(function() {
      resultView.render();
    });
  }
});

var applicationObject = new ApplicationObject();

var myRouter = new Marionette.AppRouter({
  controller: applicationObject,
  appRoutes: {
    "": "heating",
    "heating": "heating",
    "temperature": "temperature",
    "area": "area",
    "material": "material",
    "result": "result"
  }
});
