_.templateSettings = {
  interpolate: /\<\?\=(.+?)\?\>/g,
  evaluate: /\<\?(.+?)\?\>/g
};

var MyApp = new Backbone.Marionette.Application();

MyApp.on("start", function(options){
  if (Backbone.history){
    Backbone.history.start();
  }
});

MyApp.start();
