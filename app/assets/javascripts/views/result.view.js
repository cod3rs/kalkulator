var ResultView = Marionette.ItemView.extend({
  el: '#page',

  template: '#result-content'
});

var resultView = new ResultView({model: resultModel});
