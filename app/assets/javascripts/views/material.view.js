var MaterialView = Marionette.ItemView.extend({
  el: '#page',

  template: '#material-content',

  ui: {
    constructionList: '#material-construction-list',
    constructionThermalConductivity: '#material-construction-thermal-conductivity',
    constructionThickness: '#material-construction-thickness',

    thermalInsulationList: '#material-thermal-insulation-list',
    thermalInsulationThermalConductivity: '#material-thermal-insulation-thermal-conductivity',
    thermalInsulationCostOfPurchasing: '#material-thermal-insulation-cost-of-purchasing',
    thermalInsulationCostOfInsulation: '#material-thermal-insulation-cost-of-insulation'
  },

  events: {
    "change @ui.constructionList": "constructionListChanged",
    "change @ui.constructionThermalConductivity": "constructionThermalConductivityChanged",
    "change @ui.constructionThickness": "constructionThicknessChanged",

    "change @ui.thermalInsulationList": "thermalInsulationListChanged",
    "change @ui.thermalInsulationThermalConductivity": "thermalInsulationThermalConductivityChanged",
    "change @ui.thermalInsulationCostOfPurchasing": "thermalInsulationCostOfPurchasingChanged",
    "change @ui.thermalInsulationCostOfInsulation": "thermalInsulationCostOfInsulationChanged"
  },

  onRender: function() {
    var data = this.model.get("data");
    var materialConstructionList = this.model.get("materialConstructionList");
    var materialThermalInsulationList = this.model.get("materialThermalInsulationList");
    var selectedMaterialConstruction = materialConstructionList.get(data.get("materialConstructionId"));
    var selectedMaterialThermalInsulation = materialThermalInsulationList.get(data.get("materialThermalInsulationId"));

    this.ui.constructionThermalConductivity.attr("placeholder", selectedMaterialConstruction.get("thermal_conductivity"));
    this.ui.thermalInsulationThermalConductivity.attr("placeholder", selectedMaterialThermalInsulation.get("thermal_conductivity"));
    this.ui.thermalInsulationCostOfPurchasing.attr("placeholder", selectedMaterialThermalInsulation.get("cost_of_purchasing"));
    this.ui.thermalInsulationCostOfInsulation.attr("placeholder", 100);

    this.ui.constructionList.val(selectedMaterialConstruction.id);
    this.ui.constructionThermalConductivity.val(data.get("materialConstructionThermalConductivity"));
    this.ui.constructionThickness.val(data.get("materialConstructionThickness"));

    this.ui.thermalInsulationList.val(selectedMaterialThermalInsulation.id);
    this.ui.thermalInsulationThermalConductivity.val(data.get("materialThermalInsulationThermalConductivity"));
    this.ui.thermalInsulationCostOfPurchasing.val(data.get("materialThermalInsulationCostOfPurchasing"));
    this.ui.thermalInsulationCostOfInsulation.val(data.get("materialThermalInsulationCostOfInsulation"));
  },

  constructionListChanged: function(e) {
    var data = this.model.get("data");
    var materials = this.model.get("materialConstructionList");
    var selectedMaterial = materials.get(e.target.value);

    data.set("materialConstructionId", e.target.value);
    this.ui.constructionThermalConductivity.attr("placeholder", selectedMaterial.get("thermal_conductivity"));
    this.ui.constructionList.val(selectedMaterial.id);
  },

  constructionThermalConductivityChanged: function(e) {
    this.model.get("data").set("materialConstructionThermalConductivity", e.target.value);
  },

  constructionThicknessChanged: function(e) {
    this.model.get("data").set("materialConstructionThickness", e.target.value);
  },

  thermalInsulationListChanged: function(e) {
    var data = this.model.get("data");
    var materials = this.model.get("materialThermalInsulationList");
    var selectedMaterial = materials.get(e.target.value);

    data.set("materialThermalInsulationId", e.target.value);
    this.ui.thermalInsulationThermalConductivity.attr("placeholder", selectedMaterial.get("thermal_conductivity"));
    this.ui.thermalInsulationCostOfPurchasing.attr("placeholder", selectedMaterial.get("cost_of_purchasing"));
  },

  thermalInsulationThermalConductivityChanged: function(e) {
    this.model.get("data").set("materialThermalInsulationThermalConductivity", e.target.value);
  },

  thermalInsulationCostOfPurchasingChanged: function(e) {
    this.model.get("data").set("materialThermalInsulationCostOfPurchasing", e.target.value);
  },

  thermalInsulationCostOfInsulationChanged: function(e) {
    this.model.get("data").set("materialThermalInsulationCostOfInsulation", e.target.value);
  }
});

var MaterialViewModel = new Backbone.Model();

MaterialViewModel.set({
  data: dataModel,
  materialConstructionList: materialConstructionCollection,
  materialThermalInsulationList: materialThermalInsulationCollection
});

var materialView = new MaterialView({model: MaterialViewModel});
