var AreaView = Marionette.ItemView.extend({
  el: '#page',

  template: '#area-content',

  ui: {
    areaTotal: '#area-total',
    areaTotalWithoutWindows: '#area-total-without-windows'
  },

  events: {
    "change @ui.areaTotal": "areaTotalChanged",
    "change @ui.areaTotalWithoutWindows": "areaTotalWithoutWindowsChanged"
  },

  onRender: function() {
    $('#calculator').calculator({layout: $.calculator.scientificLayout});

    var data = this.model.get("data");

    this.ui.areaTotal.val(data.get("areaTotal"));
    this.ui.areaTotalWithoutWindows.val(data.get("areaWithoutWindows"));
  },

  areaTotalChanged: function(e) {
    var data = this.model.get("data");
    data.set("areaTotal", e.target.value);
  },

  areaTotalWithoutWindowsChanged: function(e) {
    var data = this.model.get("data");
    data.set("areaWithoutWindows", e.target.value);
  }
});

var AreaViewModel = new Backbone.Model();

AreaViewModel.set({
  data: dataModel,
  temperatureList: temperatureCollection.fetch()
});

var areaView = new AreaView({model: AreaViewModel});
