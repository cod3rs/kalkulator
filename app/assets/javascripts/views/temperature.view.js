var TemperatureView = Marionette.ItemView.extend({
  el: '#page',

  template: '#temperature-content',

  ui: {
    temperature: '#temperature',
    temperatureList: '#temperature-list'
  },

  events: {
    "change @ui.temperature": "temperatureChanged",
    "change @ui.temperatureList": "temperatureListChanged"
  },

  onRender: function() {
    var data = this.model.get("data");
    var temperatures = this.model.get("temperatureList");
    var selectedTemperature = temperatures.get(data.get("temperatureId"));

    this.ui.temperatureList.val(selectedTemperature.id);
    this.ui.temperature.attr("placeholder", 20);
    this.ui.temperature.val(data.get("temperature"));
  },

  temperatureChanged: function(e) {
    var data = this.model.get("data");
    data.set("temperature", e.target.value);
  },

  temperatureListChanged: function(e) {
    var data = this.model.get("data");
    var temperatures = this.model.get("temperatureList");

    var selectedTemperature = temperatures.get(e.target.value);
    data.set("temperatureId", selectedTemperature.id);
  }
});

var TemperatureViewModel = new Backbone.Model();

TemperatureViewModel.set({
  data: dataModel,
  temperatureList: temperatureCollection
});

var temperatureView = new TemperatureView({model: TemperatureViewModel});
