var HeatingListView = Marionette.ItemView.extend({
  template: '#heating-list-content',

  ui: {
    heatingList: '#heating-list'
  },

  events: {
    "change @ui.heatingList": "heatingListChanged"
  },

  onRender: function() {
    var data = this.model.get("data");
    var heatingList = this.model.get("heatingList");
    var selectedHeating = heatingList.get(data.get("heatingId"));

    this.ui.heatingList.val(selectedHeating.id);
  },

  heatingListChanged: function(e) {
    var data = this.model.get("data");
    var heatingList = this.model.get("heatingList");
    var selectedHeating = heatingList.get(e.target.value);

    data.set("heatingId", selectedHeating.id);
  }
});

var HeatingFormView = Marionette.ItemView.extend({
  template: '#heating-form-content',

  ui: {
    efficiencyLabel: '#heating-efficiency-label',
    efficiency: '#heating-efficiency',
    efficiencyMeasure: '#heating-efficiency-measure',

    priceLabel: '#heating-price-label',
    price: '#heating-price',
    priceMeasure: '#heating-price-measure',

    valueLabel: '#heating-value-label',
    value: '#heating-value',
    valueMeasure: '#heating-value-measure'
  },

  events: {
    "change @ui.efficiency": "efficiencyChanged",
    "change @ui.price": "priceChanged",
    "change @ui.value": "valueChanged"
  },

  initialize : function() {
    var data = this.model.get("data");
    this.listenTo(data, 'change', this.render);
  },

  onRender: function() {
    var data = this.model.get("data");
    var heating = this.model.get("heatingList");
    var selectedHeating = heating.get(data.get("heatingId"));

    this.ui.efficiency.attr("placeholder", selectedHeating.get("efficiency"));
    this.ui.price.attr("placeholder", selectedHeating.get("price"));
    this.ui.value.attr("placeholder", selectedHeating.get("value"));

    this.ui.efficiencyLabel.text(selectedHeating.get("efficiency_label"));
    this.ui.efficiency.val(data.get("heatingEfficiency"));
    this.ui.efficiencyMeasure.text(selectedHeating.get("efficiency_measure"));

    this.ui.priceLabel.text(selectedHeating.get("price_measure"));
    this.ui.price.val(data.get("heatingPrice"));
    this.ui.priceMeasure.text(selectedHeating.get("price_measure"));

    this.ui.valueLabel.text(selectedHeating.get("value_measure"));
    this.ui.value.val(data.get("heatingValue"));
    this.ui.valueMeasure.text(selectedHeating.get("value_measure"));
  },

  efficiencyChanged: function(e) {
    var data = this.model.get("data");
    data.set("heatingEfficiency", e.target.value);
  },

  priceChanged: function(e) {
    var data = this.model.get("data");
    data.set("heatingPrice", e.target.value);
  },

  valueChanged: function(e) {
    var data = this.model.get("data");
    data.set("heatingValue", e.target.value);
  }
});

var HeatingLayoutView = Marionette.LayoutView.extend({
  el: '#page',
  template: "#heating-content",

  regions: {
    list: "#heating-list",
    form: "#heating-form"
  },

  onRender: function() {
    this.list.show(new HeatingListView({
      model: HeatingLayoutViewModel
    }));

    this.form.show(new HeatingFormView({model: HeatingLayoutViewModel}));
  }
});

var HeatingLayoutViewModel = new Backbone.Model();

HeatingLayoutViewModel.set({
  data: dataModel,
  heatingList: heatingCollection
});

var heatingLayoutView = new HeatingLayoutView({
  model: HeatingLayoutViewModel
});
