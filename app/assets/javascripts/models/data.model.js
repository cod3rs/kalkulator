var DataModel = Backbone.Model.extend({
  defaults: {
    heatingId: 1,
    heatingEfficiency: null,
    heatingPrice: null,
    heatingValue: null,

    temperatureId: 1,
    temperature: null,

    areaTotal: null,
    areaWithoutWindows: null,

    materialConstructionId: 1,
    materialConstructionThermalConductivity: null,
    materialConstructionThickness: null,

    materialThermalInsulationId: 1,
    materialThermalInsulationThermalConductivity: null,
    materialThermalInsulationCostOfPurchasing: null,
    materialThermalInsulationCostOfInsulation: null
  }
});

var dataModel = new DataModel();
