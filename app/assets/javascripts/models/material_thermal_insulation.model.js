var MaterialThermalInsulationModel = Backbone.Model.extend({
  defaults: {
    id: null,
    name: null,
    thermal_conductivity: null,
    cost_of_purchasing: null
  },

  initialize: function() {
    this.set({'cost_of_purchasing': this.get('purchasing_cost')});
  }
});
