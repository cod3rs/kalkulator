class ResultsController < ApplicationController
  def calculate
    calculator = Calculator.new params
    data = calculator.data
    result = calculator.result

    render json: {data: data, result: result}
  end
end
