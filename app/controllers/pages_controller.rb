class PagesController < ApplicationController
  def home
    @heating = Heating.all
    @cities = City.all
    @material_constructions = MaterialConstruction.all
    @material_thermal_insulations = MaterialThermalInsulation.all
  end
end
