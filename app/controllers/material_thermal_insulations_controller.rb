class MaterialThermalInsulationsController < ApplicationController
  def index
    @material_thermal_insulations = MaterialThermalInsulation.all
    render json: @material_thermal_insulations
  end
end
