class MaterialConstructionsController < ApplicationController
  def index
    @material_constructions = MaterialConstruction.all
    render json: @material_constructions
  end
end
