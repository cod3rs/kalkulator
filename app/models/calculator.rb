class Calculator
  def initialize params
    heating_id = 1
    @heating = Heating.find(heating_id)

    temperature_id = 1
    @city = City.find(temperature_id)

    material_construction_id = 1
    @material_construction = MaterialConstruction.find(material_construction_id)

    material_thermal_insulation_id = 1
    @material_thermal_insulation = MaterialThermalInsulation.find(material_thermal_insulation_id)

    case @heating.category
      when 1
        @heating_category = 'Gazowe'
      when 2
        @heating_category = 'Pompa'
      when 3
        @heating_category = 'Ekogroszek'
      when 4
        @heating_category = 'Pelet'
      when 5
        @heating_category = 'Olejowe'
      when 6
        @heating_category = 'Ekogroszek'
      else
        @heating_category = 'Miał/Węgiel'
    end

    @heating_efficiency = params[:heating_efficiency].nil? ?
      BigDecimal(@heating.efficiency) : BigDecimal(params[:heating_efficiency])

    @heating_price = params[:heating_price].nil? ?
      @heating.price : params[:heating_price]

    @heating_value = params[:heating_value].nil? ?
      @heating.value : params[:heating_value]

    @temperature = params[:temperature].nil? ?
      20 : params[:temperature]

    @area_total = BigDecimal params[:area_total]
    @area_without_windows = BigDecimal params[:area_without_windows]

    @material_construction_thermal_conductivity = params[:material_construction_thermal_conductivity].nil? ?
      @material_construction.thermal_conductivity : params[:material_construction_thermal_conductivity]

    @material_construction_thickness = BigDecimal params[:material_construction_thickness]

    @material_thermal_insulation_thermal_conductivity = params[:material_thermal_insulation_thermal_conductivity].nil? ?
      @material_thermal_insulation.thermal_conductivity : params[:material_thermal_insulation_thermal_conductivity]

    @material_thermal_insulation_purchasing_cost = params[:material_thermal_insulation_purchasing_cost].nil? ?
      @material_thermal_insulation.purchasing_cost : params[:material_thermal_insulation_purchasing_cost]

    @material_thermal_insulation_insulation_cost = params[:material_thermal_insulation_insulation_cost].nil? ?
      100 : params[:material_thermal_insulation_insulation_cost]
  end

  def u measurement
    1 / (0.13 + ((@material_construction_thickness / 100) /
        @material_construction_thermal_conductivity) +
        ((measurement / 100) / @material_thermal_insulation_thermal_conductivity) +
        0.04);
  end

  def meets_building u
    u >= 0.3 ? 0 : 1
  end

  def meets_nf40 u
    u >= 0.2 ? 0 : 1
  end

  def basket_of_insulation measurement
    (@area_total * @material_thermal_insulation_insulation_cost) +
      ((measurement / 100) *
        @area_without_windows *
        @material_thermal_insulation_purchasing_cost)
  end

  def h u
    @area_without_windows * u
  end

  def months_result h
    sum = 0

    for i in (0...12) do
      result_all = 0.024 * (@temperature - @city[:te][i]) * @city[:ld][i] * h
      sum += result_all
    end

    sum
  end

  def heating_costs months_result
    result = 0

    case @heating.category
      when 1
        s1 = months_result / (@heating_efficiency / 100)
        s2 = @heating_value * 0.278
        s3 = @heating_price
        result = s1 / s2 * s3
      when 2
        s1 = months_result / @heating_efficiency
        s3 = @heating_price
        result = s1 * s3
      when 3
        s1 = months_result / (@heating_efficiency / 100)
        s2 = @heating_value * 0.278
        s3 = @heating_price / 1000
        result = s1 / s2 * s3
      when 4
        s1 = months_result / (@heating_efficiency / 100)
        s2 = @heating_value * 0.278
        s3 = @heating_price / 1000
        result = s1 / s2 * s3
      when 5
        s1 = months_result / (@heating_efficiency / 100)
        s3 = @heating_price
        result = s1 * s3
      when 6
        s1 = months_result / (@heating_efficiency / 100)
        s2 = @heating_value / 0.86 * 0.278;
        s3 = @heating_price
        result = s1 / s2 * s3
      else
        s1 = months_result / (@heating_efficiency / 100)
        s2 = @heating_value * 0.278
        s3 = @heating_price / 1000
        result = s1 / s2 * s3
    end

    result
  end

  def data
    {
      heating_category: @heating_category,
      heating_efficiency: @heating_efficiency,
      heating_price: @heating_price,
      heating_value: @heating_value,
      temperature: @temperature,
      area_total: @area_total,
      area_without_windows: @area_without_windows,
      material_construction_thermal_conductivity: @material_construction_thermal_conductivity,
      material_construction_thickness: @material_construction_thickness,
      material_thermal_insulation_thermal_conductivity: @material_thermal_insulation_thermal_conductivity,
      material_thermal_insulation_purchasing_cost: @material_thermal_insulation_purchasing_cost,
      material_thermal_insulation_insulation_cost: @material_thermal_insulation_insulation_cost
    }
  end

  def result
    result = []
    heating_costs_first = 0

    for i in (0..50) do
      data_result = {}

      data_result[:measurement] = BigDecimal i
      data_result[:u] = u data_result[:measurement]
      data_result[:meets_building] = meets_building data_result[:u]
      data_result[:meets_nf40] = meets_nf40 data_result[:u]

      data_result[:h] = h data_result[:u]

      data_result[:months_result] = months_result data_result[:h]

      data_result[:heating_costs] = heating_costs data_result[:months_result]

      if data_result[:measurement] >= 1
        data_result[:basket_of_insulation] = basket_of_insulation data_result[:measurement]
        data_result[:refund] = data_result[:basket_of_insulation] / (heating_costs_first - data_result[:heating_costs])
        data_result[:gain_on_warming] = heating_costs_first - data_result[:heating_costs]
        puts "basket_of_insulation: #{data_result[:basket_of_insulation]}"
        puts "refund: #{data_result[:refund]}"
        puts "gain_on_warming: #{data_result[:gain_on_warming]}"
        puts "heating_costs_first: #{heating_costs_first}"
      else
        heating_costs_first = data_result[:heating_costs]
      end

      result << data_result
    end

    result
  end
end
