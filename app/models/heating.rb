class Heating < ActiveRecord::Base
  def price
    self.read_attribute(:price).to_f
  end

  def value
    self.read_attribute(:price).to_f
  end
end
