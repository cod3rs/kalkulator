Rails.application.routes.draw do
  root 'pages#home'
  get 'api/heating', to: 'heating#index'
  get 'api/cities', to: 'cities#index'
  get 'api/material_constructions', to: 'material_constructions#index'
  get 'api/material_thermal_insulations', to: 'material_thermal_insulations#index'
  get 'api/calculate', to: 'results#calculate'
end
